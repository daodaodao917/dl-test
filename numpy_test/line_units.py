#!/usr/local/bin/python
# -*- coding: utf-8 -*-
# 多层神经元

import math
import time

from numpy_test.dataset_utils import *
from numpy_test.lost_functions import *


def dw(a, y, x):
    return np.dot((a - y) * (2 / len(a)), np.transpose(x))


def db(a, y):
    return (a - y) * (2 / len(a))


class Network(object):
    def __init__(self, weights_and_units):
        self.step = 0.01  # 步长
        self.weights_and_units = weights_and_units
        self.layer_num = np.shape(weights_and_units)[0]

        self.t_w = list(range(self.layer_num))
        self.t_b = list(range(self.layer_num))
        self.t_z = list(range(self.layer_num + 1))
        self.t_lost = 100000000000

        self.optimum_w = self.t_w
        self.optimum_b = self.t_b
        self.optimum_lost = 100000000000

        for i in range(self.layer_num):
            self.t_w[i] = np.random.randn(weights_and_units[i, 1], weights_and_units[i, 0]) * 0.01  # (m, n)的向量
            self.t_b[i] = 0

    # 线性函数
    def forward(self, x, layer_index):
        return np.dot(self.t_w[layer_index], x) + self.t_b[layer_index]

    # 验证
    def validate(self, x, y):
        for i in range(np.shape(y)[1]):
            print("y=%s, y_hat=%s" % (y[0, i], self.t_z[self.layer_num][0, i]))

    # 梯度下降训练
    def gradient_train(self, x, y):
        i = 1
        self.t_z[0] = x
        while 1:
            j = 0
            for j in range(self.layer_num):
                self.t_z[j + 1] = self.forward(self.t_z[j], j)
            _lost = ols_lost(y, self.t_z[self.layer_num])
            if math.fabs(_lost) >= math.fabs(self.t_lost):
                break

            j = self.layer_num
            for j in range(self.layer_num, 0, -1):
                _dw = dw(self.t_z[j], y, x)
                _db = db(self.t_z[j], y)
                self.t_w[j - 1] -= np.mean(np.dot(_dw, 0.01), axis=0)
                self.t_b[j - 1] -= np.mean(np.dot(_db, 0.01), axis=0)
            self.t_lost = _lost
            i += 1
        # print("i: %s, w: %s, b: %s, lost: %s" % (i, self.t_w, self.t_b, self.t_lost))

    # 多次修改使用不同的初始值训练
    def train(self, x, y, train_num=100):
        start = time.time()
        i = 1
        while i <= train_num:
            self.t_lost = 100000000000
            for j in range(self.layer_num):
                self.t_w[j] = np.random.randn(self.weights_and_units[j, 1],
                                              self.weights_and_units[j, 0]) * 0.01  # (m, n)的向量
                self.t_b[j] = 0
            self.gradient_train(x, y)
            if self.t_lost < self.optimum_lost:
                self.optimum_lost = self.t_lost
                self.optimum_w = self.t_w
                self.optimum_b = self.t_b
            i += 1
        else:
            self.validate(x, y)
            # print("w: %s, b: %s" % (self.t_w, self.t_b))
            for j in range(self.layer_num):
                print("w[%s]: %s" % (j + 1, self.t_w[j]))
                print("b[%s]: %s" % (j + 1, self.t_b[j]))
            print("损失量: %s" % self.t_lost)
            print("训练时间: %ss" % (time.time() - start))


# 2层网络
network = Network(np.array([(2, 2), (2, 1)]))  # 行数代表层数，第一层设置了两个神经元，两个特征；第二层设置了一个神经元

print("单样本")
x_list, y_list = generate_multi_dataset(1, 2)
network.train(x_list, y_list)

print()
print("多样本")
x_list, y_list = generate_multi_dataset(5, 2)  # 5组数据，2个特征
network.train(x_list, y_list)
