#!/usr/local/bin/python
# -*- coding: utf-8 -*-
# 单个神经元

import math
import time

from numpy_test.dataset_utils import *
from numpy_test.lost_functions import *


def dw(a, y, x):
    return np.dot((a - y) * (2 / len(a)), np.transpose(x))


def db(a, y):
    return (a - y) * (2 / len(a))


class Network(object):
    def __init__(self, num_of_weights):
        # np.random.seed(0)
        self.num_of_weights = num_of_weights
        self.step = 0.01  # 步长

        self.t_w = np.random.randn(1, num_of_weights) * 0.01  # (1, n)的向量
        self.t_b = 0
        self.t_lost = 100000000000

        self.optimum_w = self.t_w
        self.optimum_b = self.t_b
        self.optimum_lost = 100000000000
        # print("init w: %s" % self.w)

    # 线性函数
    def forward(self, x):
        return np.dot(self.t_w, x) + self.t_b

    # 验证
    def validate(self, x, y):
        y = transpose_row(y)
        y_hat = transpose_row(np.dot(self.t_w, x) + self.t_b)
        for i in range(np.shape(y_hat)[1]):
            print("y=%s, y_hat=%s" % (y[0, i], y_hat[0, i]))

    # 梯度下降训练
    def gradient_train(self, x, y):
        i = 1
        while 1:
            a_list = self.forward(x)
            _dw = dw(a_list, y, x)
            _db = db(a_list, y)
            _lost = ols_lost(y, a_list)
            if math.fabs(_lost) >= math.fabs(self.t_lost):
                break
            # print("%s: dw: %s, db: %s, w: %s, b: %s, lost: %s" % (i, _dw, _db, self.w, self.b, _lost))
            self.t_w -= np.mean(np.dot(_dw, 0.01), axis=0)
            self.t_b -= np.mean(np.dot(_db, 0.01), axis=0)
            self.t_lost = _lost
            i += 1
        # print("i: %s, w: %s, b: %s, lost: %s" % (i, self.t_w, self.t_b, self.t_lost))

    # 多次修改使用不同的初始值训练
    def train(self, x, y, train_num=100):
        start = time.time()
        i = 1
        while i <= train_num:
            self.t_lost = 100000000000
            self.t_w = np.random.randn(1, self.num_of_weights) * 0.01  # (1, n)的向量
            self.t_b = 0
            self.gradient_train(x, y)
            if self.t_lost < self.optimum_lost:
                self.optimum_lost = self.t_lost
                self.optimum_w = self.t_w
                self.optimum_b = self.t_b
            i += 1
        else:
            self.validate(x, y)
            print("w: %s, b: %s" % (self.t_w, self.t_b))
            print("损失量: %s" % self.t_lost)
            print("训练时间: %ss" % (time.time() - start))


network = Network(3)

print("单样本")
x_list, y_list = generate_multi_dataset(1, 3)
network.train(x_list, y_list)

print()
print("多样本")
x_list, y_list = generate_multi_dataset(10, 3)
network.train(x_list, y_list)
