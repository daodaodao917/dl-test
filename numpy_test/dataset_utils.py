#!/usr/local/bin/python
# -*- coding: utf-8 -*-

import numpy as np


# 线性
def generate_line_dataset(num=10):
    # y = ax + b
    a = np.random.randint(1, 10)
    b = np.random.randint(1, 10)
    print("line: y=%dx + %d" % (a, b))
    x_list = np.random.random((num,))
    y_list = list(map(lambda _x: a * _x + b, x_list))
    return x_list, y_list


def generate_multi_dataset(num=10, attr_num=2, min=0, max=1):
    a_list = min + (max - min) * np.random.random((1, attr_num))
    x_list = min + (max - min) * np.random.random((attr_num, num))
    y_list = np.dot(a_list, x_list)
    # print(a_list)
    # print(x_list)
    # print(y_list)
    return x_list, y_list

# generate_multi_dataset(1, 3)