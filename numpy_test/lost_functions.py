#!/usr/local/bin/python
# -*- coding: utf-8 -*-

import numpy as np


def transpose_row(x):
    return x if np.shape(x)[0] == 1 else np.transpose(x)


def transpose_column(x):
    return x if np.shape(x)[1] == 1 else np.transpose(x)


def ols_lost(y, y_hat):
    y = transpose_row(y)
    y_hat = transpose_row(y_hat)
    return np.sum(np.power(y - y_hat, 2) * (1 / np.shape(y)[0]))


def de_ols_lost(y, y_hat):
    y = transpose_row(y)
    y_hat = transpose_row(y_hat)
    return (2 / np.shape[y][0])(y - y_hat)
