#!/usr/local/bin/python
# -*- coding: utf-8 -*-
import numpy as np


def sigmoid(z):
    return 1 / (1 + np.power(np.e, -z))


def de_sigmoid(z, y):
    g = sigmoid(z)
    return g(1 - g)


def tanh(z):
    t1 = np.power(np.e, z)
    t2 = np.power(np.e, -z)
    return (t1 - t2) / (t1 + t2)


def de_tanh(z, y):
    g = tanh(z)
    return 1 - np.power(g, 2)